#ifndef LABA2_INTERFACE_H
#define LABA2_INTERFACE_H
#include <iostream>
#include "ISorter.h"
#include "LinkedList.h"
#include "LinkedListSequence.h"
#include "ArraySequence.h"
#include <ctime>
using namespace std;

template <class T>
class Interface {
private:
    Sequence<T> *items;
    ISorter<T> *sorter;
public:
    T get(int i) {
        return items->get(i);
    }

    void chooseType() {
        cout << "Choose\n0 - array\n1 - list" << endl;
        int t;
        cin >> t;
        if (t == 0) {
            items = new LinkedListSequence<T>;
        }
        if (t == 1) {
            items = new ArraySequence<T>;
        }
    }

    void chooseType(bool b) {
        if (b) {
            items = new LinkedListSequence<T>;
        }
        else {
            items = new ArraySequence<T>;
        }
    }

    void chooseSize() {
        cout << "Enter size of sequence" << endl;
        int s;
        cin >> s;
        cout << "Choose\n0 - manually\n1 - random" << endl;
        int k;
        cin >> k;
        if (k == 0) {
            this->items->create(s);
        }
        else {
            this->items->createRandom(s);
        }
    }

    void chooseSize(int s, bool b) {
        if (b) {
            this->items->create(s);
        }
        else {
            this->items->createRandom(s);
        }
    }

    void chooseSize(int s, bool b, vector<int> arr = {}) {
        if (b) {
            this->items->createRandom(s);
        }
        else {
            this->items->create(arr);
        }
    }

    void chooseSize(vector<int> arr = {}) {
        this->items->create(arr);
    }

    void chooseSort() {
        cout << "0 - bubbleSort\n"
                "1 - shakerSort\n"
                "2 - insertionSort\n"
                "3 - shellSort\n"
                "4 - pirSort\n"
                "5 - countingSort\n"
                "6 - shellDataSort" << endl;
        int t;
        cin >> t;
        if (t == 0) {
            sorter = new BubbleSorter<T>;
        }
        if (t == 1) {
            sorter = new ShakerSorter<T>;
        }
        if (t == 2) {
            sorter = new InsertionSorter<T>;
        }
        if (t == 3) {
            sorter = new ShellSorter<T>;
        }
        if (t == 4) {
            sorter = new CountingSorter<T>;
        }
        if (t == 5) {
            sorter = new PirSorter<T>;
        }
        if (t == 6) {
            sorter = new ShellDataSorter<T>;
            cout << "choose 2 numbers" << endl;
            int aa, bb;
            cin >> aa >> bb;
            int tt = clock();
            dynamic_cast<ShellDataSorter<T>*>(sorter)->sort(items, aa, bb);
            //print();
            cout << "time " << clock() - tt << endl;
            return;
        }

        int tt = clock();
        sorter->sort(items);
        //print();
        cout << "time " << clock() - tt << endl;
    }

    int chooseSort(int t) {
        if (t > 6) {
            t = 3;
        }
        if (t == 0) {
            sorter = new BubbleSorter<T>;
        }
        if (t == 1) {
            sorter = new ShakerSorter<T>;
        }
        if (t == 2) {
            sorter = new InsertionSorter<T>;
        }
        if (t == 3) {
            sorter = new ShellSorter<T>;
        }
        if (t == 4) {
            sorter = new CountingSorter<T>;
        }
        if (t == 5) {
            sorter = new PirSorter<T>;
        }
        if (t == 6) {
            sorter = new ShellDataSorter<T>;
            int aa(2), bb(3);
            int tt = clock();
            dynamic_cast<ShellDataSorter<T>*>(sorter)->sort(items, aa, bb);
            tt = clock() - tt;
            return tt;
        }
        int tt = clock();
        sorter->sort(items);
        tt = clock() - tt;
        return tt;
    }
};

#endif //LABA2_INTERFACE_H
