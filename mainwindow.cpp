#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Interface.h"
#include <qmessagebox.h>
#include <vector>
using namespace std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_3_clicked()
{
    ui->result->setText("");
    auto *a = new Interface<int>;
    bool seq;
    seq = ui->list->isChecked();
    a->chooseType(seq);
    QString size;
    size = ui->size->text();
    int sz;
    sz = size.toInt();
    bool ent;
    ent = ui->automatically->isChecked();
    //a->chooseSize(sz, ent);
    //ui->result->setText("10");

    QString numbers = ui->numbers->toPlainText();
    auto nbr = numbers.split(QLatin1Char(' '), Qt::SkipEmptyParts);

    vector<int> arr;

    if(ent) {
        ui->numbers->setPlainText("");
        for (int i = 0; i < sz; ++i) {
            int k = rand() % 1000;
            arr.push_back(k);
            ui->numbers->setPlainText(ui->numbers->toPlainText() + QString::number(k) + QLatin1Char(' '));
        }
    }
    else {
        for (int i = 0; i < nbr.size(); ++i) {
            arr.push_back(nbr[i].toInt());
        }
    }

//    for (int i = 0; i < arr.size(); ++i) {
//        ui->result->setText(ui->result->toPlainText() + QString::number(arr[i]) + QLatin1Char(' '));
//    }


    a->chooseSize(arr);

    int sort = ui->comboBox->currentIndex();

    a->chooseSort(sort);

    QMessageBox msgBox;
    msgBox.setText("time " + QString::number(a->chooseSort(sort)));


    for (int i = 0; i < arr.size(); ++i) {
        ui->result->setText(ui->result->toPlainText() + QString::number(a->get(i)) + QLatin1Char(' '));
    }

    msgBox.exec();

    //ui->result->setText(ui->result->toPlainText() + QString::number(sort) + QLatin1Char(' '));
}

void MainWindow::on_pushButton_clicked()
{
    ui->result->setText("");
    ui->size->setText("");
    ui->numbers->setPlainText("");
}
